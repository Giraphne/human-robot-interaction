# Choregraphe bezier export in Python.
from naoqi import ALProxy
names = list()
times = list()
keys = list()

names.append("HeadPitch")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[-0.16639, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.16639, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.16639, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.16639, [3, -0.444444, 0], [3, 0, 0]]])

names.append("HeadYaw")
times.append([0.666667, 1.33333, 2.66667, 4, 5.33333])
keys.append([[0, [3, -0.222222, 0], [3, 0.222222, 0]], [0, [3, -0.222222, 0], [3, 0.444444, 0]], [1.11177, [3, -0.444444, 0], [3, 0.444444, 0]], [0, [3, -0.444444, 0], [3, 0.444444, 0]], [0, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LAnklePitch")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[0.0820305, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0820305, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0820305, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0820305, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LAnkleRoll")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[-0.133578, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.133578, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.133578, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.133578, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LElbowRoll")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[-0.417394, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.417394, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.417394, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.417394, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LElbowYaw")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[-1.19381, [3, -0.444444, 0], [3, 0.444444, 0]], [-1.19381, [3, -0.444444, 0], [3, 0.444444, 0]], [-1.19381, [3, -0.444444, 0], [3, 0.444444, 0]], [-1.19381, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LHand")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[0.291819, [3, -0.444444, 0], [3, 0.444444, 0]], [0.291819, [3, -0.444444, 0], [3, 0.444444, 0]], [0.291819, [3, -0.444444, 0], [3, 0.444444, 0]], [0.291819, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LHipPitch")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[0.13, [3, -0.444444, 0], [3, 0.444444, 0]], [0.13, [3, -0.444444, 0], [3, 0.444444, 0]], [0.13, [3, -0.444444, 0], [3, 0.444444, 0]], [0.13, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LHipRoll")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[0.0922142, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0922142, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0922142, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0922141, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LHipYawPitch")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[-0.179056, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.179056, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.179056, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.179057, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LKneePitch")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[-0.084754, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.084754, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.084754, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.0847539, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LShoulderPitch")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[1.46886, [3, -0.444444, 0], [3, 0.444444, 0]], [1.45585, [3, -0.444444, 0.00610188], [3, 0.444444, -0.00610188]], [1.43225, [3, -0.444444, 0], [3, 0.444444, 0]], [1.46886, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LShoulderRoll")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[0.192342, [3, -0.444444, 0], [3, 0.444444, 0]], [0.222865, [3, -0.444444, -0.0116762], [3, 0.444444, 0.0116762]], [0.262399, [3, -0.444444, 0], [3, 0.444444, 0]], [0.192342, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LWristYaw")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[0.0916411, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0916411, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0916411, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0916411, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RAnklePitch")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[0.0914363, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0914363, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0914363, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0914364, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RAnkleRoll")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[0.1309, [3, -0.444444, 0], [3, 0.444444, 0]], [0.1309, [3, -0.444444, 0], [3, 0.444444, 0]], [0.1309, [3, -0.444444, 0], [3, 0.444444, 0]], [0.1309, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RElbowRoll")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[0.418988, [3, -0.444444, 0], [3, 0.444444, 0]], [0.34383, [3, -0.444444, 0], [3, 0.444444, 0]], [0.267748, [3, -0.444444, 0], [3, 0.444444, 0]], [0.418989, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RElbowYaw")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[1.20068, [3, -0.444444, 0], [3, 0.444444, 0]], [1.11177, [3, -0.444444, 0.00200847], [3, 0.444444, -0.00200847]], [1.18863, [3, -0.444444, -0.0120511], [3, 0.444444, 0.0120511]], [1.20068, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RHand")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[0.3, [3, -0.444444, 0], [3, 0.444444, 0]], [0.87, [3, -0.444444, 0], [3, 0.444444, 0]], [0.868949, [3, -0.444444, 0.00105101], [3, 0.444444, -0.00105101]], [0.3, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RHipPitch")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[0.13, [3, -0.444444, 0], [3, 0.444444, 0]], [0.13, [3, -0.444444, 0], [3, 0.444444, 0]], [0.13, [3, -0.444444, 0], [3, 0.444444, 0]], [0.13, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RHipRoll")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[-0.0980107, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.0980107, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.0980107, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.0980107, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RHipYawPitch")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[-0.179056, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.179056, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.179056, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.179057, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RKneePitch")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[-0.09, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.09, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.09, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.09, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RShoulderPitch")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[1.46696, [3, -0.444444, 0], [3, 0.444444, 0]], [0.718793, [3, -0.444444, 0.01058], [3, 0.444444, -0.01058]], [0.708213, [3, -0.444444, 0], [3, 0.444444, 0]], [1.46696, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RShoulderRoll")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[-0.179627, [3, -0.444444, 0], [3, 0.444444, 0]], [0.314159, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.107933, [3, -0.444444, 0.0716944], [3, 0.444444, -0.0716944]], [-0.179628, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RWristYaw")
times.append([1.33333, 2.66667, 4, 5.33333])
keys.append([[0.102526, [3, -0.444444, 0], [3, 0.444444, 0]], [1.6104, [3, -0.444444, 0], [3, 0.444444, 0]], [1.60778, [3, -0.444444, 0.00262361], [3, 0.444444, -0.00262361]], [0.102526, [3, -0.444444, 0], [3, 0, 0]]])

try:
  # uncomment the following line and modify the IP if you use this script outside Choregraphe.
  # motion = ALProxy("ALMotion", IP, 9559)
  motion = ALProxy("ALMotion")
  motion.angleInterpolationBezier(names, times, keys)
except BaseException, err:
  print err
