import nao_nocv_2_0 as nao
import time
import math
import landmark as l

IP="192.168.0.115" #marvin
#IP="192.168.0.112" #bender
#IP="127.0.0.1" #local

# connect to and initialize robot
nao.InitProxy(IP)
nao.InitPose()
nao.InitSonar()

def computeDistance(markerinfo):
    landmarkSize = 0.10
    #dx = markerinfo[0][3]
    #dy = markerinfo[0][4]
    angletheta = markerinfo[0][5]
    # which function is used here?
    distanceToLandmark = landmarkSize / ( 2 * math.tan( angletheta / 2))
    print "Distance is", distanceToLandmark
    print "dx, dy: ", dx, dy
    return distanceToLandmark

# maybe rename to rotateLandmark?
def findPerson(landmark, dx):
    count = 0
    maxcount = 1500
    # if the painting has been on the lefthandside for nao, turn right to face the person.
    moveRight = dx < 0
    
    # a while loop is needed because we don't know how long it takes to find the human.    
    while count < maxcount:
        count=count+1
        # once every 300 ticks, do a searching operation
        if count%300 == 0:
            # stop walking so the nao doesn't fall over
            nao.Move(0.0, 0, 0)
            try:
                markerinfo = l.detectLandmark(landmark)      
                #Check if the appropriate landmark is found
                if markerinfo[0][0] == landmark:
                    return True

            # if the landmark cannot be found, walk.
            except AssertionError as e:
                nao.Move(-0.2, 0, -0.2 if moveRight else 0.2)
                time.sleep(0.2)
                continue
        
        # if we're not checking for presence, keep turning.
        nao.Move(-0.2, 0, -0.2 if moveRight else 0.2)
        time.sleep(0.2)
    # if the landmark is not found at the end of the loop, return false  
    return False      

def computeTurnrate(SL, SR, dtheta, previoustheta):
    dtheta = SL - SR
    # moderate the new sonardata with the previous direction
    rotation = (dtheta + previoustheta)/2
    print "Theta is ", rotation
    return rotation

def limitTurnRate(turnrate):
    if turnrate >0.2:
        turnrate =0.2
    elif turnrate<-0.2:
        turnrate=-0.2
    return turnrate

def walkStraight():
    print "straight ahead"
    nao.Move(0.5,0,0)
    time.sleep(0.2)

def turn(spot, SL, SR):
    goRight = SL <= SR
    return -0.2 if goRight else 0.2
    #if spot:
        #nao.Move(-0.2, 0, -0.2 if goRight else 0.2)
        #return -0.2 if goRight else 0.2
        #print "turning on the spot"
    #else:
        #print "seeing wall, going {}".format("right" if goRight else "left")
        #nao.Move(0.2, 0, -0.2 if goRight else 0.2)
        #return -0.2
    #time.sleep(0.2)

def combineForces(obstacle, landmark):
    # combine the forces for landmark detection and obstacle avoidance
    # these can be weighted in different proportions.
    return ((obstacle) + landmark)/2

def moveTo(landmark=0):
    # define all variables needed.
    count = 0
    maxcount = 4000
    # initially, all angles and measurements are zero.
    dtheta = 0.0
    previoustheta = 0.0
    obstacle = 0
    landmarktheta = 0
    turn = False

    while count < maxcount:
        count=count+1

        # use the sonar data to calculate a route for obstacle avoidance
        [SL, SR]=nao.ReadSonar()
        print "Sonardata: ", SL, SR
        obstacletheta = computeTurnrate(SL, SR, dtheta, previoustheta)
        # set the current value as previous value for the new round
        previoustheta = obstacletheta
        smallestDistance = min(SL, SR)

        # Treat all special cases first.
        # stop if too close to obstacle
        # return true if target, return false if other obstacle.
        if smallestDistance<0.2:
            print "Stopped"
            nao.Move(0.0, 0, 0)
            nao.Crouch()
            print "Crouched"
            break
        #return True

        # if there is nothing close, just walk straight
        if SL > 2.5 and SR > 2.5:
            # if there are no obstacles, then we don't have to avoid anything, so angle is 0
            obstacletheta = 0
            
        # if we are so close that normal turning does not work
        # turn on the spot.
        elif abs(SL-SR) < 0.4 and SL < 0.6 and SR < 0.6:
            obstacletheta = turn(True, SL, SR)
            turn = True
            #continue

        # if were close to a wall and were walking straight into it,
        # go slower and turn.
        elif abs(SL-SR) < 0.2 and SL < 0.9 and SR < 0.9:
            obstacletheta = turn(False, SL, SR)
            #continue

        #if we are not looking for a landmark, just walk normally.
        if landmark == 0:
            rotation = limitTurnRate(obstacletheta)
            if turn:
                nao.Move(-0.2, 0, rotation)
                time.sleep(0.2)
                turn = False
            else: 
                nao.Move(0.5, 0, rotation)
                time.sleep(0.2)
            continue

        # otherwise, find the correct landmark and move towards it.
        elif count%150 == 0:
            try:
                nao.Move(0.0, 0, 0)
                markerinfo = l.detectLandmark(landmark)
                print markerinfo

                # compute the distance to the marker.
                distance = computeDistance(markerinfo)

                # check if target is reached
                if distance < 0.6 and landmark == 114:
                    break
                elif distance < 0.6:
                    nao.Move(0.0, 0, 0)
                    print "looking for person"
                    # as long as the function does not return true, keep searching
                    while not findPerson(114, markerinfo[0][1]):
                        print "Hello person!"
                        # TODO: this needs to be removed.
                        nao.Crouch()
                        break
                
                # if the target is not reached, keep walking
                # compute the movement force for the marker
                landmarktheta = 0.5*markerinfo[0][1] # only works with one landmark
                #compute the weighted turnrate
                rotation = combineForces(obstacle, landmark)
                # store the angle for the landmark for the loops that we don't measure location
                landmarktheta = rotation
                rotation = limitTurnRate(rotation)

                # head in the right direction
                nao.Move(0.7, 0, rotation)   
                print "Current rotation is ", rotation
                time.sleep(0.2)

            # if no marker was found by the function, just do obstacle avoidance
            except AssertionError as e:
                print e.args
                rotation = limitTurnRate(obstacletheta)
                nao.Move(0.5, 0, rotation)
                time.sleep(0.2)
                continue
        # if we are in the while loop without searching,
        # move while incorporating the previous location of the landmark and any obstacles you might encounter.
        else:
            rotation = combineForces(obstacletheta, landmarktheta)
            rotation = limitTurnRate(rotation)
            nao.Move(0.5, 0, rotation)
            time.sleep(0.2)

    #Stop if time is up            
    nao.Move(0.0, 0, 0)
    nao.Crouch()

if __name__=="__main__":
    moveTo(64)
