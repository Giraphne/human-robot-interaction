# -*- coding: cp1252 -*-
import nao_nocv_2_0 as nao
import time
import random
#IP="192.168.0.115" #marvin
IP="192.168.0.118" #bender
#IP="127.0.0.1" #local

# connect to and initialize robot
nao.InitProxy(IP)
#nao.InitPose()
#nao.asr.unsubscribe("MyModule")
def saidYes():
    wordList = ["yes","no"]
    language="English"
    nao.InitSpeech(wordList, language)
    nao.asr.subscribe("MyModule")
    print 'Speech recognition engine started'
    count=0
    maxcount=400
    while count<maxcount:
        result=nao.DetectSpeech()
        if len(result)>0:
            print result[0]
            if result[0]=="yes":
                nao.asr.unsubscribe("MyModule")
                #nao.Say("You said "+result[0])
                return True
            else:
                nao.asr.unsubscribe("MyModule")
                #nao.Say("You said "+result[0])
                return False
        else:
            print "I did not understand."
            nao.Say("� did not understand")
            time.sleep(1.5)
        count=count+1  
if __name__=="__main__":
    speechDetect()

