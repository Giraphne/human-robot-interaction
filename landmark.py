import nao_nocv_2_0 as nao
import time

def detectLandmark(gain=1.0, offset=[0.0, -0.2]):
    """
    It looks for a mark and if it finds it returns an array with the markerinfo
    markerinfo[0]		#markerID
    markerinfo[1]		#alpha - x location in camera angle
    markerinfo[2]		#beta  - y location
    markerinfo[3]		#sizeX
    markerinfo[4]		#sizeY
    markerinfo[5] 	        #orientation about vertical w.r. Nao's head
    """
    
    nao.InitLandMark()

    #Check for landmark
    detected, timestamp, markerinfo = nao.DetectLandMark()

    #If the landmark is detected, return the landmark info
    if detected:
        print('Found marker!')
        return markerinfo

    #If marker is not instantly found, look for the marker by moving the head
    max_yaw=1.5 #range [-119, 119] degrees
    min_yaw=-1.5

    #Max turn values
    max_pitch=0.26 # conservative but safe, real range is [-38.5, 29.5] /180*Pi
    min_pitch=-0.6

    #Arrays to loop through when moving the head (different head positions)
    yaw =   [0.0, 0.0,-0.2,-0.2,0.0,0.2, 0.2, 0.2, 0.0,-0.2,-0.4,-0.4,-0.4,-0.2,0.0,0.2,0.4, 0.4, 0.4, 0.2, 0.0]
    pitch = [0.0,-0.2,-0.2, 0.0,0.0,0.0,-0.2,-0.4,-0.4,-0.4,-0.4,-0.2, 0.0, 0.0,0.0,0.0,0.0,-0.2,-0.4,-0.4,-0.4]

    #Loop through all the head angles (turn the head)                               
    for i in range(0,len(yaw)):
        names      = ["HeadYaw", "HeadPitch"]
        the_yaw=yaw[i]*gain+offset[0]
        the_pitch=pitch[i]*gain+offset[1]
        if the_yaw>max_yaw:
            the_yaw=max_yaw
        if the_yaw<min_yaw:
            the_yaw=min_yaw
        if the_pitch>max_pitch:
            the_pitch=max_pitch
        if the_pitch<min_pitch:
            the_pitch=min_pitch
        
        nao.MoveHead(the_yaw, the_pitch, isAbsolute=True, timeLists=[[1],[1]])
        time.sleep(1)

        #Check if a marker is detected with the new head position
        detected, timestamp, markerinfo = nao.DetectLandMark()

        #If the marker is detected, return the landmark info
        if detected:
            #Reset the head to the normal position
            nao.MoveHead(0, 0, isAbsolute=True, timeLists=[[1],[1]])
            print('Found marker!')
            return markerinfo

    #When done looping through all the head positions but nothing found, return False. 
    print('Niks gevonden')
    nao.MoveHead(0, 0, isAbsolute=True, timeLists=[[1],[1]])
    raise AssertionError("No landmark found")
    return False


if __name__=="__main__":
    detectLandmark()
