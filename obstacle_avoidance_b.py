import nao_nocv_2_0 as nao
import time
import math

#IP="192.168.0.115" #marvin
#IP="192.168.0.118" #bender
IP="127.0.0.1"

# connect to and initialize robot
nao.InitProxy(IP)
nao.InitPose()
#nao.InitLandMark()
nao.InitSonar()

def computeTurnrate(SL, SR):
    return SL-SR

#Init all variables
count = 0
maxcount = 2000 # make sure the loop ends
dtheta = 0.0
previoustheta = 0.0
#Start main code for obstacle avoidance
while count < maxcount:
    count=count+1

    #### Sonar ####
    [SL, SR]=nao.ReadSonar()

    # if there is nothing close, just walk straight
    if SL > 2.5 and SR > 2.5:
        print "straight ahead"
        nao.Move(1.0,0,0)
        time.sleep(0.2)
        continue

    # if we are so close that normal turning does not work
    # turn on the spot
    if abs(SL-SR) < 0.4 and SL < 0.6 and SR < 0.6:
        goRight = SL <= SR
        print "turning on the spot"
        nao.Move(-0.2, 0, -0.2 if goRight else 0.2)
        time.sleep(0.2)
        continue

    # if were close to a wall and were walking straight into it,
    # go slower and turn right.
    if abs(SL-SR) < 0.2 and SL < 0.9 and SR < 0.9:
        goRight = SL <= SR
        print "seeing wall, going {}".format("right" if goRight else "left")
        nao.Move(0.2, 0, -0.2 if goRight else 0.2)
        time.sleep(0.2)
        continue

    print SL, SR
    smallestDistance = min(SL, SR)

    # stop if too close to obstacle
    if smallestDistance<0.3:
        print SL,SR
        print "Stopped"
        nao.Move(0.0, 0, 0)
        nao.Crouch()
        print "Crouched"
        break

        
#compute the turnrate
    dtheta = computeTurnrate(SL, SR)
    rotation = (dtheta + previoustheta)/2
    print "Theta is ", rotation
    previoustheta = dtheta
   # dtheta = dtheta - theta

    # limit the turnrate
    if rotation >0.2:
        rotation =0.2
    elif rotation<-0.2:
        rotation=-0.2
    
    # move the robot
    nao.Move(1.0,0,rotation) # head in the right direction  
    time.sleep(0.2) # you should not send too many commands to the robot, so wait 0.1s

#Stop if time is up            
nao.Move(0.0,0,0)
nao.Crouch()


