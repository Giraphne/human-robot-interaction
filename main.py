# import statements
from naoqi import ALProxy
import nao_nocv_2_0 as nao
import time
import math
import random as rand
# import own libraries
import move as m
import speech as s
#import gestures as g
#import landmark as l

#globals variables
#IP="192.168.0.115" #marvin
IP="192.168.0.118" #bender
#IP="127.0.0.1" #localhost

tts = ALProxy("ALTextToSpeech", IP, 9559)
motionProxy = ALProxy("ALMotion", IP, 9559)

def leadTo(target):
    nao.RunMovement("FollowMeRyu.py")
    # move towards target # m.moveTo(target)
    # check following
    # reach target
    # position correctly # how to do this??
    return True

def describe(piece):
    if piece == 1:
        nao.RunMovement ("ExplainArtRyu.py")
        tts.say("So, this is our first artwork. It is called: \\rspd=90\\ Entry of Alexander into Babylon")
        # -- nao position is on the right side of tourist and the left side of the art --
        tts.say("This painting is the work of \\rspd=80\\ Charles Le Brun \\pau=600\\ during the 17th century")
    elif piece == 2:
        nao.RunMovement ("ExplainArtRyu.py")
        tts.say("This next piece is called \\rspd=90\\ Apollo and Daphne.")
        # -- nao position is on the right side of tourist and the left side of the art --
        tts.say("This artwork was sculptured by Italian artist \\rspd=90\\ Gian Lorenzo Bernini.")
        tts.say("The painting is about a battle between Daphne and the \\vct=90\\ sexual desires of Apollo.")
    elif piece == 3:
        nao.RunMovement ("ExplainArtRyu.py")
        tts.say("This is our main exhibition: \\rspd=90\\ The Mona Lisa")
        tts.say("This painting is made by Italian artist \\rspd=90\\ Leonardo da Vinci")
        tts.say("For years and years, it is been debated whether the woman in the painting might be \\rspd=80\\ Lisa Gherardini.")
        tts.say("\\vct=110\\ \\rspd=80\\ Only da Vinci knows the truth.")
        
    # look at piece thoughtfully
    nao.RunMovement("LookThoroughlyRyu.py")
    time.sleep(5)
    # turn towards human
    nao.RunMovement ("AskMoreStory.py")
    tts.say("Do you want to hear more stories about this piece?")
    
    saidYes = s.saidYes()

    if piece == 1 and saidYes:
        nao.RunMovement ("ExplainArtRyu.py")
        tts.say("So, actually, there are four paintings of the series of this story: namely \\rspd=90\\ the Passage of the Granicus, the Battle of Argela, the Entry of Alexander into Babylon and Alexander and Porus")
        tts.say("This painting that you see in front of you right now, was considered by \\rspd=90\\ Le Brun himself \\rspd=100\\ to be his masterpiece. In this painting, \\rspd=90\\ Le Brun \\rspd=100\\ depicted Alexander standing in a chariot drawn by two elephants, \\pau=800\\ making his triumphant entry into \\rspd=90\\ Babylon")
        tts.say("This was an important moment in the life of \\rspd=90\\ Alexander the third of Macedonia, \\pau=800\\ also known as Alexander The Great")
    elif piece == 2 and saidYes:
        nao.RunMovement ("ExplainArtRyu.py")
        tts.say("Maybe some of you already know that in Greek Mythology, \\pau=800\\ Apollo was the God of Light. He has also been referred to as the God of music, \\pau=800\\ and many others.")
        tts.say("Daphne was a \\rspd=90\\ Naiad Nymph \\rspd=100\\ and was the daughter of a river God. She was famous for being incredibly beautiful and for catching the eye of Apollo")
        tts.say("Daphne was determined to remain unmarried and untouched by a man. However, Apollo had been really desperate for Daphne")
        tts.say("\\vct=110\\ Daphne is forced to sacrifice her body \\vct=100\\ and become the laurel tree as her only form of escape from Apollo.")
    elif piece == 3 and saidYes:
        nao.RunMovement ("ExplainArtRyu.py")
        tts.say("This painting has long been caricaturized in cartoons, \\pau=800\\ has been replicated all over the world, \\pau=800\\ and has been studied by scholars and art enthusiasts alike.")
        tts.say("The painting is the most widely recognized work of art \\vct=110\\ in the entire world.")
        tts.say("This piece of Renaissance artwork completely changed both the techniques, and style of painting.")
        tts.say("The Mona Lisa bears a strong resemblance to many Renaissance depictions of the Virgin Mary, who was at that time seen as \\vct=110\\ an ideal \\vct=100\\ for womanhood.")
        tts.say("Today the Mona Lisa is considered the most famous painting in the world, \\pau=800\\ but until the 20th century it was simply one among many highly regarded artworks.")
        tts.say("Did you know, \\pau=800\\ that X-rays have shown there are three different versions of the Mona Lisa hidden under the present one?")
        
    tts.say("What do you think, do you like it?")
    # not sure this works.
    tts.say(randomReaction(s.saidYes()))

def farewell():
    tts.say("\\vct=120\\Thank you very much for your attention.")
    tts.say("\\vct=120\\ Did you enjoy your visit?")
    if not s.saidYes():
        nao.InitPose()
        tts.say("\\vct=80\\ Maybe next time will be better")
        time.sleep(2)
    else:
        tts.say("Great to hear, you are welcome to visit anytime!")
        tts.say("\\vct=120\\ Please come again!")

    nao.RunMovement("ByeRyu.py")
    tts.say("Bye")
    
    returnState()
    return "Done"
    # The other option is to return to "Introduction", 
    # but then the code doesn't end.

def init():
    # connect to and initialize robot
    nao.InitProxy(IP)
    nao.InitPose()
    nao.InitLandMark()
    nao.InitSonar()
    return "Introduction"

def returnState():
    # move back to the starting point #m.moveTo(startnumber)
    print "return"

def introState():   
    # finding a face 
    # approach human # m.moveTo(usernr)
    # attract attention 
    nao.RunMovement("HelloRyu")
    tts.say("\\rspd=90\\ Hello everyone, good day!")
    nao.RunMovement("PoliteRyu")
    tts.say("\\rspd=90\\ I am Nao. \\pau=800\\ I am the tour guide in this IPO museum.")
    nao.RunMovement("IntroduceRyu")
    tts.say("Would you like a tour guide to explain more details about the art in this museum?")
    nao.RunMovement("OfferFirstTourRyu.py")
    
    if not s.saidYes():
        # no interest in tour
        tts.say("Okay, enjoy your visit!")
        returnState()
    
    tts.say("We offer both a full tour \\pau=600\\ and a showing of our main art piece.")
    tts.say("Would you like to do the whole tour?")
    if s.saidYes():
        tts.say("\\vct=120\\ Okay, please follow me!")
        return "Grand"
    else:
        tts.say("Okay, then I will take you to our main exhibit, \\pau=800\\ \\rspd=90\\ the Mona Lisa.")
        return "Main_piece"


def grandTour(item):
    if item == 1:
        if leadTo(1): describe(1)
        if leadTo(2): describe(2)
    if leadTo(3): describe(3)

    return "Farewell"

def randomReaction(happy):
    if happy:
        reactions = ["\\vct=120\\ I like it too! \\vct=100\\", "\\vct=120\\ I think it is gorgeous! \\vct=100\\"]
        index = int(math.floor(rand.random()*2))
        return reactions[index]    
    else:
        reactions = ["\\vct=90\\To each their own. \\vct=100\\", "\\vct=90\\ I hope our other pieces are to your liking.\\vct=100\\"]
        index = int(math.floor(rand.random()*2))
        return reactions[index]

#main
def main():
    state="Init"
    while not state=="Done":
        if state=="Introduction":
            state=introState()
        elif state=="Grand":
            state=grandTour(1)
        elif state=="Main_piece":
            state=grandTour(3)
        elif state=="Farewell":
            state=farewell()
        elif state=="Init":
            state=init()
    #now state is Done
    nao.Crouch()


if __name__=="__main__":
    main()