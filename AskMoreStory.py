# Choregraphe bezier export in Python.
# File name : Ask More Story
from naoqi import ALProxy
names = list()
times = list()
keys = list()

names.append("HeadPitch")
times.append([2.66667, 4, 5.33333])
keys.append([[-0.158868, [3, -0.888889, 0], [3, 0.444444, 0]], [-0.16639, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.158868, [3, -0.444444, 0], [3, 0, 0]]])

names.append("HeadYaw")
times.append([1, 2.66667, 4, 5.33333])
keys.append([[0, [3, -0.333333, 0], [3, 0.555556, 0]], [1.01927, [3, -0.555556, 0], [3, 0.444444, 0]], [0, [3, -0.444444, 0], [3, 0.444444, 0]], [1.01753, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LAnklePitch")
times.append([2.66667, 4, 5.33333])
keys.append([[0.0820305, [3, -0.888889, 0], [3, 0.444444, 0]], [0.0820305, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0820305, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LAnkleRoll")
times.append([2.66667, 4, 5.33333])
keys.append([[-0.133578, [3, -0.888889, 0], [3, 0.444444, 0]], [-0.133578, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.133578, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LElbowRoll")
times.append([2.66667, 4, 5.33333])
keys.append([[-0.422799, [3, -0.888889, 0], [3, 0.444444, 0]], [-0.417394, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.422799, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LElbowYaw")
times.append([2.66667, 4, 5.33333])
keys.append([[-1.20079, [3, -0.888889, 0], [3, 0.444444, 0]], [-1.19381, [3, -0.444444, 0], [3, 0.444444, 0]], [-1.20079, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LHand")
times.append([2.66667, 4, 5.33333])
keys.append([[0.290459, [3, -0.888889, 0], [3, 0.444444, 0]], [0.291819, [3, -0.444444, 0], [3, 0.444444, 0]], [0.290459, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LHipPitch")
times.append([2.66667, 4, 5.33333])
keys.append([[0.13, [3, -0.888889, 0], [3, 0.444444, 0]], [0.13, [3, -0.444444, 0], [3, 0.444444, 0]], [0.13, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LHipRoll")
times.append([2.66667, 4, 5.33333])
keys.append([[0.0922142, [3, -0.888889, 0], [3, 0.444444, 0]], [0.0922141, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0922142, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LHipYawPitch")
times.append([2.66667, 4, 5.33333])
keys.append([[-0.179056, [3, -0.888889, 0], [3, 0.444444, 0]], [-0.179057, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.179056, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LKneePitch")
times.append([2.66667, 4, 5.33333])
keys.append([[-0.084754, [3, -0.888889, 0], [3, 0.444444, 0]], [-0.0847539, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.084754, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LShoulderPitch")
times.append([2.66667, 4, 5.33333])
keys.append([[1.43414, [3, -0.888889, 0], [3, 0.444444, 0]], [1.43225, [3, -0.444444, 0], [3, 0.444444, 0]], [1.43383, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LShoulderRoll")
times.append([2.66667, 4, 5.33333])
keys.append([[0.230453, [3, -0.888889, 0], [3, 0.444444, 0]], [0.2624, [3, -0.444444, 0], [3, 0.444444, 0]], [0.230932, [3, -0.444444, 0], [3, 0, 0]]])

names.append("LWristYaw")
times.append([2.66667, 4, 5.33333])
keys.append([[0.0973308, [3, -0.888889, 0], [3, 0.444444, 0]], [0.0916411, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0973308, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RAnklePitch")
times.append([2.66667, 4, 5.33333])
keys.append([[0.0914363, [3, -0.888889, 0], [3, 0.444444, 0]], [0.0914364, [3, -0.444444, 0], [3, 0.444444, 0]], [0.0914363, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RAnkleRoll")
times.append([2.66667, 4, 5.33333])
keys.append([[0.1309, [3, -0.888889, 0], [3, 0.444444, 0]], [0.1309, [3, -0.444444, 0], [3, 0.444444, 0]], [0.1309, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RElbowRoll")
times.append([2.66667, 4, 5.33333])
keys.append([[0.435137, [3, -0.888889, 0], [3, 0.444444, 0]], [0.267747, [3, -0.444444, 0], [3, 0.444444, 0]], [0.429251, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RElbowYaw")
times.append([2.66667, 4, 5.33333])
keys.append([[1.20756, [3, -0.888889, 0], [3, 0.444444, 0]], [1.18863, [3, -0.444444, 0], [3, 0.444444, 0]], [1.21044, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RHand")
times.append([2.66667, 4, 5.33333])
keys.append([[0.3, [3, -0.888889, 0], [3, 0.444444, 0]], [0.868949, [3, -0.444444, 0], [3, 0.444444, 0]], [0.309102, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RHipPitch")
times.append([2.66667, 4, 5.33333])
keys.append([[0.13, [3, -0.888889, 0], [3, 0.444444, 0]], [0.13, [3, -0.444444, 0], [3, 0.444444, 0]], [0.13, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RHipRoll")
times.append([2.66667, 4, 5.33333])
keys.append([[-0.0980107, [3, -0.888889, 0], [3, 0.444444, 0]], [-0.0980107, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.0980107, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RHipYawPitch")
times.append([2.66667, 4, 5.33333])
keys.append([[-0.179056, [3, -0.888889, 0], [3, 0.444444, 0]], [-0.179057, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.179056, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RKneePitch")
times.append([2.66667, 4, 5.33333])
keys.append([[-0.09, [3, -0.888889, 0], [3, 0.444444, 0]], [-0.09, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.09, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RShoulderPitch")
times.append([2.66667, 4, 5.33333])
keys.append([[1.43528, [3, -0.888889, 0], [3, 0.444444, 0]], [0.708213, [3, -0.444444, 0], [3, 0.444444, 0]], [1.4297, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RShoulderRoll")
times.append([2.66667, 4, 5.33333])
keys.append([[-0.217027, [3, -0.888889, 0], [3, 0.444444, 0]], [-0.107933, [3, -0.444444, 0], [3, 0.444444, 0]], [-0.211899, [3, -0.444444, 0], [3, 0, 0]]])

names.append("RWristYaw")
times.append([2.66667, 4, 5.33333])
keys.append([[0.109296, [3, -0.888889, 0], [3, 0.444444, 0]], [1.60778, [3, -0.444444, 0], [3, 0.444444, 0]], [0.107834, [3, -0.444444, 0], [3, 0, 0]]])

try:
  # uncomment the following line and modify the IP if you use this script outside Choregraphe.
  # motion = ALProxy("ALMotion", IP, 9559)
  motion = ALProxy("ALMotion")
  motion.angleInterpolationBezier(names, times, keys)
except BaseException, err:
  print err
